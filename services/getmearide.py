from inspect import signature
import Pyro5.api
import Pyro5.nameserver
from cryptography.hazmat.primitives.serialization import base
from unique_id import get_unique_id
from models.user import User
import base64

from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.exceptions import InvalidSignature

users = []
rides = []
interests = []

def find(id, arr):
    for i in range(len(arr)):
        if id == arr[i][0]:
            return i

@Pyro5.api.expose
class GetMeARideService(object):        
    def register_user(self, name, phone, pem):
        pem_data = base64.b64decode(pem["data"])
        pub_key = serialization.load_pem_public_key(pem_data)

        self.user = User(name, phone, pub_key)
        if(any(x.phone == phone for x in users)):
            print('The user was already registered.')
            return False
        else:
            users.append(self.user)
            print('The user was successfully registered.')
            return True

    def list_rides(self, origin, destination, date):
        found_rides = [(ride_user, ride_origin, ride_destination, ride_date) 
                        for (ride_id, ride_user, ride_origin, ride_destination, ride_passengers, ride_date) 
                        in rides 
                        if ride_origin == origin and
                        ride_destination == destination and 
                        ride_date == date]
        for ride in found_rides:
            if(ride[0].uri != None):
                remote_uri = ride[0].uri
                remote = Pyro5.api.Proxy(remote_uri)
                remote.notify_passenger_interested(self.user.name,self.user.phone,ride[1],ride[2],ride[3])
        return [(ride_user.name, ride_user.phone, ride_origin, ride_destination, ride_date) for (ride_user, ride_origin, ride_destination, ride_date) in found_rides]


    def register_ride(self, uri, origin, destination, passengers, date, enc_signature):
        signature = base64.b64decode(enc_signature['data'])
        try:
            interest_data = bytes(str(uri if uri != None else None) + ';' + origin + ';' + destination + ';' + date, 'utf-8')
            
            self.user.pub_key.verify(
                signature,
                interest_data,
                padding.PSS(
                    mgf=padding.MGF1(hashes.SHA256()),
                    salt_length=padding.PSS.MAX_LENGTH
                ),
                hashes.SHA256()
            )
            self.user.uri = uri
            id = get_unique_id(length=4, excluded_chars='}{][:*^`\",.~;%+-')
            rides.append([id, self.user, origin, destination, passengers, date])
            if(any(x[2] == origin and x[3] == destination and x[4] == date for x in interests)):
                remote_uri = [x for x in interests if x[2] == origin and x[3] == destination and x[4] == date][0][1].uri
                if(remote_uri != None):
                    remote = Pyro5.api.Proxy(remote_uri)
                    remote.notify_ride_available(self.user.name, self.user.phone, origin, destination, date)
            print('The ride {0} was successfully registered.'.format(id))
            return id
        except InvalidSignature:
            print("The signature is Invalid!")

    def register_interest(self, uri, origin, destination, date, enc_signature):
        signature = base64.b64decode(enc_signature['data'])
        try:
            interest_data = bytes(str(uri if uri != None else None) + ';' + origin + ';' + destination + ';' + date, 'utf-8')

            self.user.pub_key.verify(
                signature,
                interest_data,
                padding.PSS(
                    mgf=padding.MGF1(hashes.SHA256()),
                    salt_length=padding.PSS.MAX_LENGTH
                ),
                hashes.SHA256()
            )
            self.user.uri = uri
            id = get_unique_id(length=4, excluded_chars='}{][:*^`\",.~;%+-')
            interests.append([id, self.user, origin, destination, date])
            print('The interest {0} was successfully registered.'.format(id))
            return id
        except InvalidSignature:
            print("The signature is Invalid!")

    def cancel_interest(self, interest_id):
        index = find(interest_id, interests)
        if(index != None):
            interests.pop(index)
            print('The interest {0} was successfully cancelled.'.format(interest_id))
            return True
        return False

    def cancel_ride(self, ride_id):
        index = find(ride_id, rides)
        if(index != None):
            rides.pop(index)
            print('The ride {0} was successfully cancelled.'.format(ride_id))
            return True
        return False