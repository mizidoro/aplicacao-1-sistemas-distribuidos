from inspect import signature
import Pyro5.api
import Pyro5.core
import threading
from Pyro5.server import expose, oneway
from getkey import getkey
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.exceptions import InvalidSignature


class Client(object):
    def __init__(self):
        self.ns = Pyro5.core.locate_ns()
        self.uri = self.ns.lookup("get.me.a.ride")
        self.ride_service = Pyro5.api.Proxy(self.uri)    # use name server object lookup uri shortcut
        self.notification_open = False
        self.rides = []
        self.interests = []
        self.private_key = ...
        self.public_key = ...

    @expose
    @oneway
    def notify_ride_available(self, name, phone, origin, destination, date):
        self.notification_open = True
        self.print_header()
        print('Someone offered a ride that you are interested! Get in touch and plan your trip!\n')
        print('Trip information: From: {0} || To: {1} || Date: {2} ({3} : {4})'.format(origin, destination, date, name, phone))
        self.exec_return()

    @expose
    @oneway
    def notify_passenger_interested(self, name, phone, origin, destination, date):
        self.notification_open = True
        self.print_header()
        print('Someone is interested in one of your rides! Get in touch and plan your trip!\n')
        print('Trip information: From: {0} || To: {1} || Date: {2} ({3} : {4})'.format(origin, destination, date, name, phone))
        self.exec_return()

    def key_generate(self):
        self.private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
            backend=default_backend()
        )
        self.public_key = self.private_key.public_key()

    def exec_return(self):
        option = 0
        print('\nX - Return\n\n> ', end='')
        while(option != 'X'):
            option = getkey()
            if(option == 'X' or option == 'x'):
                option = 0
                break

    def exec_menu(self):
        self.print_header()
        self.name = input("What is your name? ").strip()
        self.phone = input("And your phone number? ").strip()
        
        self.key_generate()
        
        pem = self.public_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )

        if(not self.ride_service.register_user(self.name, self.phone, pem)):
            self.print_header()
            print("\nThe informed phone is already in use. Exiting...\n")
        else:
            option = 0

            while(option >= 0):
                self.print_options()
            
                while(option != 'x' and (option < 1 or option > 4)):
                    print('\r> ', end='')
                    option = getkey()
                    try:
                        option = int(option)
                    except:
                        option = -1 if option != 'X' and option != 'x' else 'x'

                if(option == 'x'):
                    if(not self.notification_open):
                        print('X')
                        break
                    else:
                        self.notification_open = False

                if(option == 1):
                    self.exec_find_ride_option()

                if(option == 2):
                    self.exec_register_ride_option()

                if(option == 3):
                    self.exec_cancel_interest()

                if(option == 4):
                    self.exec_cancel_ride()

                option = 0

    def print_header(self):
        print('\033c', end="")
        print('-------------------------------')
        print('-------- Get me a Ride --------')
        print('-------------------------------')   
        print('\n')

    def print_options(self):
        self.print_header() 
        print('Choose one of the following:')
        print('\n')
        print('1 - Find a ride.')
        print('2 - Offer a ride.')
        print('3 - Cancel an interest.')
        print('4 - Cancel a ride.')
        print('X - Exit.')
        print('\n')

    def exec_find_ride_option(self):
        self.print_header()
        origin = input("What's the origin of yout trip: ")
        destination = input("What's the destination of yout trip: ")
        date = input("What's the date of your trip (dd/mm/yyyy): ")
        notify = input('Do you want to get notified when someone offers a ride to {0}? (y/n): '.format(destination))
        notify = True if notify == 'y' else False

        interest_data = bytes(str(self.uri if notify else None) + ';' + origin + ';' + destination + ';' + date, 'utf-8')
        
        signature = self.private_key.sign(
            interest_data,
            padding.PSS(
                mgf=padding.MGF1(hashes.SHA256()),
                salt_length=padding.PSS.MAX_LENGTH),
            hashes.SHA256())
        
        interest_id = self.ride_service.register_interest(self.uri if notify else None, origin, destination, date, signature)
        self.interests.append([interest_id, origin, destination, date])
        rides = self.ride_service.list_rides(origin, destination, date)
        if(len(rides) > 0):
            print('\nThe following rides were found:\n')
            for ride in rides:
                print('From: {0} || To: {1} || Date: {2} ({3} : {4})'.format(ride[2],ride[3],ride[4],ride[0],ride[1]))
        else:
            print("\nNo rides from {0} to {1} were found on this date.\n".format(origin, destination))
        self.exec_return()

    def exec_register_ride_option(self):
        self.print_header()
        origin = input('From where are you leaving: ')
        destination = input("Where are you going to: ")
        passengers = input("How many passengers are going: ")
        date = input("What's the date of your trip (dd/mm/yyyy): ")
        notify = input('Do you want to get notified when someone could be interested in your ride? (y/n): ')
        notify = True if notify == 'y' else False

        interest_data = bytes(str(self.uri if notify else None) + ';' + origin + ';' + destination + ';' + date, 'utf-8')
        
        signature = self.private_key.sign(
            interest_data,
            padding.PSS(
                mgf=padding.MGF1(hashes.SHA256()),
                salt_length=padding.PSS.MAX_LENGTH),
            hashes.SHA256())

        ride_id = self.ride_service.register_ride(self.uri if notify else None, origin, destination, passengers, date, signature)
        self.rides.append([ride_id, origin, destination, passengers, date])
        print('\nYour trip was registered.')
        self.exec_return()

    def exec_cancel_interest(self):
        self.print_header()
        if(len(self.interests) > 0):
            print('Your ride interests:\n')
            idx = 0
            for interest in self.interests:
                print('{0} - Id: {1} || From: {2} || To: {3} || Date: {4}'.format(idx, interest[0], interest[1], interest[2], interest[3]))
                idx = idx + 1
            interest_idx = int(input('\nType the interest number that you want to cancel: '))
            if(self.ride_service.cancel_interest(self.interests[interest_idx][0])):
                self.interests.pop(interest_idx)
        else:
            print("You don't have any ride interests.")
        self.exec_return()

    def exec_cancel_ride(self):
        self.print_header()
        if(len(self.rides) > 0):
            print('Your rides:\n')
            idx = 0
            for ride in self.rides:
                print('{0} - Id: {1} || From: {1} || To: {2} || Passengers: {3} || Date: {4}'.format(idx, ride[0], ride[1], ride[2], ride[3], ride[4]))
                idx = idx + 1
            ride_idx = int(input('\nType the ride number that you want to cancel: '))
            if(self.ride_service.cancel_ride(self.rides[ride_idx][0])):
                self.rides.pop(ride_idx)
        else:
            print("You don't have any rides scheduled.")
        self.exec_return()

class ClientDaemon(threading.Thread):
    def __init__(self, client):
        threading.Thread.__init__(self)
        self.cli = client
        self.setDaemon(True)
    def run(self):
        with Pyro5.server.Daemon() as daemon:
            self.cli.uri = daemon.register(self.cli)
            daemon.requestLoop()

cli = Client()

cli_daemon = ClientDaemon(cli)
cli_daemon.start()

cli.exec_menu()