import Pyro5.api
import Pyro5.nameserver
import threading
from services.getmearide import GetMeARideService

class NameServer(threading.Thread):
    def run(self):
        Pyro5.nameserver.start_ns_loop()

nameserver = NameServer()
nameserver.start()

daemon = Pyro5.server.Daemon()              # make a Pyro daemon
ns = Pyro5.api.locate_ns()                  # find the name server
uri = daemon.register(GetMeARideService)    # register the ride service as a Pyro object
ns.register("get.me.a.ride", uri)           # register the object with a name in the name server

print("Ready.")
daemon.requestLoop()                        # start the event loop of the server to wait for calls